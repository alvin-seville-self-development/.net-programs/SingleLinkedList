﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace SingleLinkedList
{
    /// <summary>
    /// Строготипизированный односвязный список.
    /// </summary>
    /// <typeparam name="T">Тип элементов списка.</typeparam>
    [DebuggerTypeProxy(typeof(CollectionDebugView<>))]
    [DebuggerDisplay("Count = {Count}")]
    [Serializable]
    public class SingleLinkedList<T> : ICollection<T>, IReadOnlyCollection<T>, ICollection
    {
        private int version;
        private SingleLinkedListNode<T> tail;

        private void ThrowInvalidOperationExceptionIfNull()
        {
            if (Count == 0)
                throw new InvalidOperationException(nameof(Count));
        }

        private void ThrowExceptionIfInvalidArrayOrIndex(Array array, int index)
        {
            if (ReferenceEquals(array, null))
                throw new ArgumentNullException(nameof(array));
            if ((index < 0) || (index >= array.Length))
                throw new IndexOutOfRangeException(nameof(index));
            if (Count > array.Length - index)
                throw new ArgumentException(nameof(index));
        }
        
        private void CopyToArray(Array array, int index)
        {
            SingleLinkedListNode<T> current = First;
            while (!ReferenceEquals(current, null))
            {
                array.SetValue(current.Value, index++);
                current = current.Next;
            }
        }

        private SingleLinkedListNode<T> Find(T item)
        {
            SingleLinkedListNode<T> current = First;
            while (!ReferenceEquals(current, null) && !current.Value.Equals(item))
                current = current.Next;
            return current;
        }

        private SingleLinkedListNode<T> FindPrevious(T item)
        {
            SingleLinkedListNode<T> previous = First;
            while (!ReferenceEquals(previous.Next, null) && !previous.Next.Value.Equals(item))
                previous = previous.Next;
            return previous;
        }

        /// <summary>
        /// Синхронизированная оболочка.
        /// </summary>
        public class Synchronized : ICollection<T>
        {
            private SingleLinkedList<T> source;

            private object syncRoot;

            public int Count
            {
                get
                {
                    lock (syncRoot)
                        return source.Count;
                }
            }

            public bool IsReadOnly
            {
                get
                {
                    lock (syncRoot)
                        return source.IsReadOnly;
                }
            }

            internal Synchronized(SingleLinkedList<T> singleLinkedList)
            {
                source = singleLinkedList;
                syncRoot = singleLinkedList.SyncRoot;
            }

            public void Add(T item)
            {
                lock (syncRoot)
                    source.AddLast(item);
            }

            public bool Remove(T item)
            {
                lock (syncRoot)
                    return source.Remove(item);
            }

            public void Clear()
            {
                lock (syncRoot)
                    source.Clear();
            }

            public bool Contains(T item)
            {
                lock (syncRoot)
                    return source.Contains(item);
            }

            public void CopyTo(T[] array, int arrayIndex)
            {
                lock (syncRoot)
                    source.CopyTo(array, arrayIndex);
            }

            public IEnumerator<T> GetEnumerator()
            {
                lock (syncRoot)
                    return source.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                lock (syncRoot)
                    return source.GetEnumerator();
            }
        }

        /// <summary>
        /// Объект синхронизации.
        /// </summary>
        public object SyncRoot { get; } = new object();

        /// <summary>
        /// Указывает является ли коллекция синхронизированной.
        /// </summary>
        public bool IsSynchronized => false;

        /// <summary>
        /// Указывает является ли коллекция коллекцией с доступом только на чтение.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Количество элементов коллекции.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Голова списка.
        /// </summary>
        public SingleLinkedListNode<T> First { get; private set; }

        /// <summary>
        /// Конструирует односвязный список из коллекции.
        /// </summary>
        /// <param name="collection">Коллекция.</param>
        public SingleLinkedList(IEnumerable<T> collection)
        {
            if (ReferenceEquals(collection, null))
                throw new ArgumentNullException(nameof(collection));
            foreach (var item in collection)
                AddLast(item);
        }

        /// <summary>
        /// Конструирует пустой односвязный список.
        /// </summary>
        public SingleLinkedList()
        {
        }

        /// <summary>
        /// Вставляет элемент первым в коллекцию.
        /// </summary>
        /// <param name="item">Вставляемый элемент.</param>
        public void AddFirst(T item)
        {
            First = new SingleLinkedListNode<T>(item, First);
            if (Count == 0)
                tail = First;
            Count++;
            version++;
        }

        /// <summary>
        /// Вставляет элемент последним в коллекцию.
        /// </summary>
        /// <param name="item">Вставляемый элемент.</param>
        public void AddLast(T item)
        {
            if (Count == 0)
                First = tail = new SingleLinkedListNode<T>(item);
            else
            {
                tail.Next = new SingleLinkedListNode<T>(item);
                tail = tail.Next;
            }
            Count++;
            version++;
        }

        /// <summary>
        /// Удаляет первый элемент коллекции.
        /// </summary>
        public void RemoveFirst()
        {
            ThrowInvalidOperationExceptionIfNull();
            First = First.Next;
            if (Count == 1)
                tail = null;
            Count--;
            version++;
        }

        /// <summary>
        /// Удаляет последний элемент коллекции.
        /// </summary>
        public void RemoveLast()
        {
            ThrowInvalidOperationExceptionIfNull();
            if (Count == 1)
                Clear();
            else
            {
                SingleLinkedListNode<T> current = First;
                while (!ReferenceEquals(current.Next, tail))
                    current = current.Next;
                current.Next = null;
                tail = current;
                Count--;
                version++;
            }
        }

        /// <summary>
        /// Вставляет элемент после другого.
        /// </summary>
        /// <param name="target">Элемент, после которого требуется произвести вставку.</param>
        /// <param name="item">Вставляемый элемент.</param>
        public void InsertAfter(T target, T item)
        {
            ThrowInvalidOperationExceptionIfNull();
            SingleLinkedListNode<T> node = Find(target) ?? throw new ArgumentException(nameof(target));
            node.Next = new SingleLinkedListNode<T>(item, node.Next);
            Count++;
            version++;
        }

        /// <summary>
        /// Вставляет элемент перед другим.
        /// </summary>
        /// <param name="target">Элемент, перед которым требуется произвести вставку.</param>
        /// <param name="item">Вставляемый элемент.</param>
        public void InsertBefore(T target, T item)
        {
            ThrowInvalidOperationExceptionIfNull();
            if (First.Value.Equals(target))
                First = new SingleLinkedListNode<T>(item, First);
            else
            {
                SingleLinkedListNode<T> previous = FindPrevious(item);
                if (previous == tail)
                    throw new ArgumentException(nameof(target));
                previous.Next = new SingleLinkedListNode<T>(item, previous.Next);
            }
            Count++;
            version++;
        }

        /// <summary>
        /// Удаляет элемент с указанным значением.
        /// </summary>
        /// <param name="item">Значение.</param>
        /// <returns>true, если удаление прошло успешно, иначе - false.</returns>
        public bool Remove(T item)
        {
            ThrowInvalidOperationExceptionIfNull();
            if (First.Value.Equals(item))
                First = First.Next;
            else
            {
                SingleLinkedListNode<T> previous = FindPrevious(item);
                if (previous == tail)
                    return false;
                previous.Next = previous.Next.Next;
            }
            Count--;
            version++;
            return true;
        }

        /// <summary>
        /// Проверяет наличие элемента в коллекции.
        /// </summary>
        /// <param name="item">Искомый элемент.</param>
        /// <returns>true, если элемент есть в коллекции, иначе - false.</returns>
        public bool Contains(T item)
        {
            return !ReferenceEquals(Find(item), null);
        }

        /// <summary>
        /// Очищает коллекцию.
        /// </summary>
        public void Clear()
        {
            ThrowInvalidOperationExceptionIfNull();
            First = tail = null;
            Count = 0;
            version++;
        }

        /// <summary>
        /// Копирует коллекцию в массив, начиная с указанного индекса.
        /// </summary>
        /// <param name="array">Массив.</param>
        /// <param name="index">Индекс.</param>
        public void CopyTo(T[] array, int index)
        {
            ThrowExceptionIfInvalidArrayOrIndex(array, index);
            CopyToArray(array, index);
        }

        /// <summary>
        /// Копирует коллекцию в массив, начиная с указанного индекса.
        /// </summary>
        /// <param name="array">Массив.</param>
        /// <param name="index">Индекс.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            if (array.Rank != 1)
                throw new RankException(nameof(array));
            ThrowExceptionIfInvalidArrayOrIndex(array, index);
            CopyToArray(array, index);
        }

        /// <summary>
        /// Вставляет элемент последним в коллекцию.
        /// </summary>
        /// <param name="item">Вставляемый элемент.</param>
        void ICollection<T>.Add(T item)
        {
            AddLast(item);
        }

        /// <summary>
        /// Возвращает перечислитель коллекции.
        /// </summary>
        /// <returns>Перечислитель коллекции.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Возвращает перечислитель коллекции.
        /// </summary>
        /// <returns>Перечислитель коллекции.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Возвращает перечислитель коллекции.
        /// </summary>
        /// <returns>Перечислитель коллекции.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Возвращает синхронизированную оболочку для коллекции.
        /// </summary>
        /// <returns>Синхронизированная оболочка для коллекции.</returns>
        public Synchronized AsSynchronized()
        {
            return new Synchronized(this);
        }

        /// <summary>
        /// Перечислитель.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            private readonly int version;

            private SingleLinkedList<T> source;

            private SingleLinkedListNode<T> current;

            private int index;

            public void ThrowWhenCollectionWasChanged()
            {
                if (version != source.version)
                    throw new InvalidOperationException("Коллекция была изменена");
            }

            /// <summary>
            /// Текущий элемент.
            /// </summary>
            public T Current => current.Value;

            /// <summary>
            /// Текущий элемент.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return Current;
                }
            }

            /// <summary>
            /// Конструирует перечислитель из указанного односвязного списка.
            /// </summary>
            /// <param name="singleLinkedList">Односвязный список.</param>
            public Enumerator(SingleLinkedList<T> singleLinkedList) : this()
            {
                version = singleLinkedList.version;
                source = singleLinkedList ?? throw new ArgumentNullException(nameof(singleLinkedList));
                index = -1;
                version = singleLinkedList.version;
            }

            /// <summary>
            /// Перемещает перечислитель к следующему элементу.
            /// </summary>
            /// <returns>true, если перемещение дальше возможно, иначе - false.</returns>
            public bool MoveNext()
            {
                ThrowWhenCollectionWasChanged();
                if (index == -1)
                    current = source.First;
                else
                    current = current.Next;
                return ++index < source.Count;
            }

            /// <summary>
            /// Переустанавливает перечислитель в начальную позицию.
            /// </summary>
            public void Reset()
            {
                ThrowWhenCollectionWasChanged();
                index = -1;
            }

            /// <summary>
            /// Высобождает неуправляемые ресурсы, используемые объектом.
            /// </summary>
            public void Dispose()
            {
            }
        }
    }
}
