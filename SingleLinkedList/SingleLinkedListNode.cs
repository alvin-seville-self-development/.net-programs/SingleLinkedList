﻿using System;

namespace SingleLinkedList
{
    /// <summary>
    /// Узел односвязного списка.
    /// </summary>
    /// <typeparam name="T">Тип значения.</typeparam>
    [Serializable]
    public class SingleLinkedListNode<T>
    {
        /// <summary>
        /// Значение.
        /// </summary>
        public T Value { get; internal set; }

        /// <summary>
        /// Следующий узел.
        /// </summary>
        public SingleLinkedListNode<T> Next { get; internal set; }

        /// <summary>
        /// Конструирует узел из значения и ссылки на следующий узел.
        /// </summary>
        /// <param name="value">Значение.</param>
        /// <param name="next">Следующий узел.</param>
        public SingleLinkedListNode(T value, SingleLinkedListNode<T> next = null)
        {
            Value = value;
            Next = next;
        }

        /// <summary>
        /// Возвращает строковое представление объекта Node.
        /// </summary>
        /// <returns>Строковое представление объекта Node.</returns>
        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
