﻿using System;
using System.Linq;

namespace SingleLinkedList
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            SingleLinkedList<int> singleLinkedList = new SingleLinkedList<int>(Enumerable.Range(1, 10));
            foreach (var item in singleLinkedList)
                Console.WriteLine(item);
        }
    }
}
